<?php

include($_SERVER['DOCUMENT_ROOT'].'modules/db_config.php');

class DB {
    static function get() {
        static $db = null;
        if ($db == null) {
            $db = new DB();
        }
        return $db;
    }
    function __construct() {
        $this->connect = mysql_connect(DB_HOST, DB_USER, DB_PASSWORD);
        if(!$this->connect) {
            die('FAIL: '.mysql_error());
        }
        mysql_select_db('ircdb');
    }
    function __destruct() {
        mysql_close($this->connect);
    }
    public function query($sql) {
        $result = mysql_query($sql, $this->connect);
        if(!$result) {
            die('Could not get data ('.$sql.'): '.mysql_error());
        }
        $rows = array();
        while($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
            $rows []= $row;
        }
        return $rows;
    }
    public function escape($string) {
        return mysql_real_escape_string($string);
    }
    public function test() {
        $sql = 'SELECT COUNT(*) as `count` FROM `irc_list`';
        $result = $this->query($sql)[0]['count'];
        print_r($result);
    }
}

?>
