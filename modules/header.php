<!DOCTYPE html>

<html lang="en">

<head>
  <title>Wildfire Games: IRC logs</title>
  <meta charset="utf-8" />
  <meta name="description" content="A global group of volunteer game developers. Our projects are the Pyrogenesis 3D game engine and the game 0 A.D." />
  <meta name="viewport" content="width=device-width" />
  <link rel="author" href="Wildfire Games" />
  <link rel="shortcut icon" href="https://www.wildfiregames.com/favicon.ico" type="image/x-icon" />
  <link rel="stylesheet" href="https://www.wildfiregames.com/css/style.css" type="text/css">
  <link rel="stylesheet" href="https://www.wildfiregames.com/css/orbit.css" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Gentium+Basic%7COpen+Sans:700' rel='stylesheet' type='text/css'>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
  <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <link rel="stylesheet" href="/css/irc_style.css" type="text/css">
<script type="text/javascript">
function reset_lines() {
    $('.table-line:visible:even').css('background-color', 'rgba(0, 0, 0, 0.05)');
    $('.table-line:visible:odd').css('background-color', 'rgba(0, 0, 0, 0)');
}
$(document).ready(function() {
    reset_lines();
    $("#hide-bots-checkbox").click(function() {
        $(".bot").toggleClass("hidden");
        reset_lines();
    });
    $("#search-date-form").submit(function() {
        var value = $("#search-year").val() + "-" + $("#search-month").val() + "-" + $("#search-day").val();
        $("input[name=date]").val(value);
        return true;
    });
});
</script>
</head>

<body>
	
	<header style="position: relative;">
		<div class="content">
			<img src="https://www.wildfiregames.com/img/logo.png" alt="Wildfire Games" />
			<nav>
				<ul>
				<li><a href="https://www.play0ad.com/">0 A.D.</a>
				<li><a href="http://trac.wildfiregames.com/">Development</a>
				<li><a href="https://www.wildfiregames.com/forum/">Forums</a>
				<li><a href="https://www.wildfiregames.com/irc.html">Chat</a>
				</ul>
			</nav>
			<br style="clear: both;">
		</div>
	</header>

	<div class="content">
<div style="clear:both;"></div>
<div>
<br/>
<h1>IRC chat logs for 0 A.D. channels</h1>
<br/>
</div>
<div style="clear:both;"></div>

<div class="searchbox">
<div class="search-date">
Request log from a specific day: <br/>
<form id="search-date-form" method="get"><select id="search-year"><?php
$current_year = intval(date("Y"));
for ($year = $current_year; $year >= 2012; --$year) {
    $selected = $year == $current_year ? ' selected' : '';
    echo '<option value="', $year, '"'.$selected.'>', $year, '</option>';
}
?></select>&nbsp;<select id="search-month"><?php
for ($month = 1; $month <= 12; ++$month) {
    $selected = $month == 1 ? ' selected' : '';
    echo '<option value="', $month, '"'.$selected.'>', $month, '</option>';
}
?></select>&nbsp;<select id="search-day"><?php
for ($day = 1; $day <= 31; ++$day) {
    $selected = $day == 1 ? ' selected' : '';
    echo '<option value="', $day, '"'.$selected.'>', $day, '</option>';
}
?></select><input type="hidden" name="date" value=""/>&nbsp;&nbsp;<button id="search-date-button">Go</button></form><br/>
</div>
<div class="search-name">
Search all logs by username:<br/>
<form id="seatch-name-form" method="get"><input type="text" name="name" placeholder="Username"/>&nbsp;<button id="search-name-button">Go</button></form><br/>
</div>
<div class="search-search">
Search all logs by a query:<br/>
<form id="seatch-query-form" method="get"><input type="text" name="query" placeholder="Query"/>&nbsp;<button id="search-query-button">Go</button></form>
</div>
</div>

<br/>

<div class="irclogs">
