<?php

function url_remove_param($url, $varname) {
    list($urlpart, $qspart) = array_pad(explode('?', $url), 2, '');
    parse_str($qspart, $qsvars);
    unset($qsvars[$varname]);
    $newqs = http_build_query($qsvars);
    return $urlpart . '?' . $newqs;
}

function irc_is_bot($name) {
    return $name == 'WildfireBot' || $name == 'WildfireBot`' || $name == 'wfgbot';
}

function irc_format_navigation_date($navigation) {
    $url = url_remove_param($_SERVER['REQUEST_URI'], 'date');
    $today = date('Y-m-d');
    $date = $navigation['date'];
    $prev_date = $navigation['prev_date'];
    $next_date = $navigation['next_date'];
    echo '<div class="date-navigation">';
    if ($today != $prev_date) {
        echo '<a href="'.$url.'&date='.$prev_date.'"><</a>&nbsp;';
    }
    echo $date;
    if ($today != $next_date) {
        echo '&nbsp;<a href="'.$url.'&date='.$next_date.'">></a>';
    }
    echo '</div>';
}

function irc_format_navigation($navigation) {
    $url = url_remove_param($_SERVER['REQUEST_URI'], 'page');
    $page = $navigation['page'];
    $current_page = $page;
    $first_page = $navigation['first_page'];
    $last_page = $navigation['last_page'];
    $prev_page = $navigation['prev_page'];
    $next_page = $navigation['next_page'];
    $pages = array();
    if ($page > $first_page) {
        $pages []= array('number' => $prev_page, 'name' => '<');
    }
    for ($i = $first_page; $i <= min($first_page + 1, $current_page); ++$i)
        $pages []= array('number' => $i, 'name' => $i + 1);
    for ($i = max($first_page, $current_page - 2); $i <= min($last_page, $current_page + 2); ++$i)
        if (count($pages) == 0 || $i > $pages[count($pages) - 1]['number'])
            $pages []= array('number' => $i, 'name' => $i + 1);
    for ($i = max($last_page - 1, $current_page); $i <= $last_page; ++$i)
        if (count($pages) == 0 || $i > $pages[count($pages) - 1]['number'])
            $pages []= array('number' => $i, 'name' => $i + 1);
    if ($page < $last_page) {
        $pages []= array('number' => $next_page, 'name' => '>');
    }

    echo '<div class="page-navigation">';
    for ($i = 0; $i < count($pages); ++$i) {
        $page = $pages[$i];
        if ($page['name'] != '<' && $page['name'] != '>' && $i > 1) {
            if ($pages[$i - 1]['number'] + 1 < $page['number']) {
                echo '&nbsp;...';
            }
        }
        if ($i > 0)
            echo '&nbsp';
        if ($page['number'] != $current_page)
            echo '<a href="'.$url.'&page='.$page['number'].'">'.$page['name'].'</a>';
        else
            echo $page['name'];
    }
    echo '</div>';
}

function irc_format_text($text) {
    $text = ' '.$text.' ';
    $text = preg_replace('/\s(D[0-9]+)\s/', ' <a href="https://code.wildfiregames.com/$1">$1</a> ', $text);
    $text = preg_replace('/\s(rP[0-9]+)\s/', ' <a href="https://code.wildfiregames.com/$1">$1</a> ', $text);
    $text = preg_replace('/\s#([0-9]+)\s/', ' <a href="https://trac.wildfiregames.com/ticket/$1">#$1</a> ', $text);
    return substr($text, 1, strlen($text) - 2);
}

function irc_format_day($list) {
    echo '<div><h3>Logs from ', $list['date'], '</h3></div>';
    echo '<div><label><input id="hide-bots-checkbox" type="checkbox" checked> Hide messages from bots</label><br/><br/></div>';
    irc_format_navigation_date($list['navigation']);
    echo '<div>';
    echo '<table cellspacing="0" class="irc_day"><tbody>';
    foreach ($list['items'] as $item) {
        $classes = '';
        if (irc_is_bot($item['name'])) {
            $classes .= ' bot hidden';
        }
        echo '<tr class="table-line'.$classes.'">';
        //echo '<td class="date">', $item['date'], '</td>';
        echo '<td class="time">', substr($item['time'], 0, 5), '</td>';
        echo '<td class="name"><a href="?name='.urlencode($item['name']).'">', $item['name'], '</a></td>';
        echo '<td class="test">', irc_format_text($item['text']), '</td>';
        echo '</tr>';
    }
    echo '</tbody></table>';
    echo '</div>';
    irc_format_navigation_date($list['navigation']);
}

function irc_format_name($list) {
    echo '<div><h3>Logs from ', $list['name'], '</h3></div>';
    irc_format_navigation($list['navigation']);
    echo '<div>';
    echo '<table cellspacing="0" class="irc_day"><tbody>';
    foreach ($list['items'] as $item) {
        echo '<tr class="table-line">';
        echo '<td class="date"><a href="?date='.$item['date'].'">', $item['date'], '</a></td>';
        echo '<td class="time">', substr($item['time'], 0, 5), '</td>';
        echo '<td class="name">', $item['name'], '</td>';
        echo '<td class="test">', irc_format_text($item['text']), '</td>';
        echo '</tr>';
    }
    echo '</tbody></table>';
    echo '</div>';
    irc_format_navigation($list['navigation']);
}

function irc_format_query($list) {
    echo '<div><h3>Logs by query "', $list['query'], '"</h3></div>';
    irc_format_navigation($list['navigation']);
    echo '<div>';
    echo '<table cellspacing="0" class="irc_day"><tbody>';
    foreach ($list['items'] as $item) {
        echo '<tr class="table-line">';
        echo '<td class="date"><a href="?date='.$item['date'].'">', $item['date'], '</a></td>';
        echo '<td class="time">', substr($item['time'], 0, 5), '</td>';
        echo '<td class="name"><a href="?name=', $item['name'], '">', $item['name'], '</a></td>';
        echo '<td class="test">', irc_format_text($item['text']), '</td>';
        echo '</tr>';
    }
    echo '</tbody></table>';
    echo '</div>';
    irc_format_navigation($list['navigation']);
}
?>
