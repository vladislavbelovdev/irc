<?php
date_default_timezone_set('GMT');
setlocale(LC_TIME, 'en_US');

require_once($_SERVER['DOCUMENT_ROOT'].'modules/db.php');

function clamp($current, $min, $max) {
    return max($min, min($max, $current));
}

class IRC {
    function __construct() {
        $this->db = DB::get();
    }

    function __destruct() {

    }

    public function test() {
        $this->db->test();
    }

    public function get_last_day() {
        $last_day = $this->db->query('SELECT `date` FROM `irc_list` ORDER BY `date` DESC LIMIT 0, 1')[0]['date'];
        return $this->get_by_date($last_day);
    }

    public function get_by_name($name, $page_size, $page) {
        $page = intval($page);
        $page_size = intval($page_size);
        if ($page < 0)
            $page = 0;
        if ($page_size < 10 || $page_size > 1000)
            $page_size = 10;
        $unescaped_name = $name;
        $name = $this->db->escape($name);
        $count = $this->db->query('SELECT COUNT(*) as `count` FROM `irc_list` WHERE `name` = "'.$name.'"')[0]['count'];
        $last_page = max(intval(($count + $page_size - 1) / $page_size) - 1, 0);
        $navigation = array(
            'page' => $page,
            'page_size' => $page_size,
            'prev_page' => max(0, $page - 1),
            'next_page' => min($last_page, $page + 1),
            'first_page' => 0,
            'last_page' => $last_page
        );
        $result = $this->db->query('SELECT * FROM `irc_list` WHERE `name` = "'.$name.'" ORDER BY `date` DESC LIMIT '.($page_size * $page).','.$page_size);
        return array(
            'name' => $unescaped_name,
            'items' => $result,
            'navigation' => $navigation
        );
    }

    public function get_by_date($date) {
        $date = $this->db->escape($date);
        $valid = true;
        $date = explode('-', $date);
        if (count($date) == 3) {
            $year = clamp(intval($date[0]), 2012, 3333);
            $month = clamp(intval($date[1]), 1, 12);
            $day = clamp(intval($date[2]), 1, 31);
            $date = sprintf('%04d-%02d-%02d', $year, $month, $day);
        } else {
            $valid = false;
        }
        if (!$valid) {
            return array(
                'date' => '',
                'items' => array(),
                'navigation' => array()
            );
        }
        $prev_date = date('Y-m-d', strtotime($date.' -1 day'));
        $next_date = date('Y-m-d', strtotime($date.' +1 day'));
        $navigation = array(
            'date' => $date,
            'prev_date' => $prev_date,
            'next_date' => $next_date
        );
        $result = $this->db->query('SELECT * FROM `irc_list` WHERE `date` = "'.$date.'" ORDER BY `date` ASC');
        return array(
            'date' => $date,
            'items' => $result,
            'navigation' => $navigation
        );
    }

    public function get_by_query($query, $page_size, $page) {
        $page = intval($page);
        $page_size = intval($page_size);
        if ($page < 0)
            $page = 0;
        if ($page_size < 10 || $page_size > 1000)
            $page_size = 10;
        $unescaped_query = $query;
        $query = $this->db->escape($query);
        $count = $this->db->query('SELECT COUNT(*) as `count` FROM `irc_list` WHERE `text` LIKE "'.$query.'"')[0]['count'];
        $last_page = max(intval(($count + $page_size - 1) / $page_size) - 1, 0);
        $navigation = array(
            'page' => $page,
            'page_size' => $page_size,
            'prev_page' => max(0, $page - 1),
            'next_page' => min($last_page, $page + 1),
            'first_page' => 0,
            'last_page' => $last_page
        );
        $result = $this->db->query('SELECT * FROM `irc_list` WHERE `text` LIKE "'.$query.'" ORDER BY `date` DESC LIMIT '.($page_size * $page).','.$page_size);
        return array(
            'query' => $unescaped_query,
            'items' => $result,
            'navigation' => $navigation
        );
    }
}

?>
