#!/usr/bin/env python
import os
import re
import sys

import MySQLdb

print(sys.version)

class DB:
    def __init__(self):
        self.db = MySQLdb.connect(host='localhost', user='ircuser', passwd='iWvtBLUXyPgZz88h', db='ircdb', charset='utf8')
        self.cursor = self.db.cursor()

        '''
        CREATE TABLE `places` (
          `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
            `name` varchar(255) NOT NULL,
              `type` varchar(255) NOT NULL,
                PRIMARY KEY (`id`)
                )
        '''
        self.insert_query = ''

    def __del__(self):
        self.db.close()

    def execute(self, sql):
        try:
            self.cursor.execute(sql)
            self.db.commit()
        except MySQLdb.Error as e:
            print(sql)
            self.db.rollback()
            #if e[0] != '###':
            #    raise
            print(str(e), e)

    def fetch_all(self):
        return self.cursor.fetchall()

    def get_tables(self):
        sql = 'SHOW TABLES FROM `ircdb`'
        self.execute(sql)
        tables = []
        for line in self.fetch_all():
            tables.append(line[0])
        return tables

    def create_table(self):
        if 'irc_list' in self.get_tables():
            return

        sql = '''CREATE TABLE `irc_list` (
        `date` DATE NOT NULL,
        `time` TIME NOT NULL,
        `line` int(8) unsigned NOT NULL,
        `channel` VARCHAR(16) NOT NULL,
        `name` VARCHAR(32) NOT NULL,
        `type` VARCHAR(8) NOT NULL,
        `text` TEXT,
        PRIMARY KEY(`date`, `time`, `line`)
        )'''
        self.execute(sql)

        sql = 'CREATE INDEX index_name ON `irc_list` (`name`)'
        self.execute(sql)
        sql = 'CREATE INDEX index_date ON `irc_list` (`date`)'
        self.execute(sql)
        sql = 'CREATE INDEX index_type ON `irc_list` (`type`)'
        self.execute(sql)

    def add_item(self, params):
        item = '("%s", "%s", %d, "%s", "%s", "%s", "%s")' % (
            params['date'],
            params['time'],
            params['line'],
            MySQLdb.escape_string(params['channel']),
            MySQLdb.escape_string(params['name']),
            params['type'],
            MySQLdb.escape_string(params['text'])
        )
        if self.insert_query:
            self.insert_query += ','
        self.insert_query += item

    def commit_insert(self):
        sql = 'INSERT INTO `irc_list` VALUE ' + self.insert_query
        self.execute(sql)
        self.insert_query = ''

    def drop_table(self):
        print(self.get_tables())
        sql = 'SELECT COUNT(*) FROM `irc_list`'
        self.execute(sql)
        for line in self.fetch_all():
            print(line)

        sql = 'DROP TABLE `irc_list`'
        self.execute(sql)

db = DB()
db.create_table()
'''
db.add_item({
    'date': '2017-12-31',
    'time': '10:00:00',
    'line': 1,
    'name': 'me',
    'type': 'MESSAGE',
    'text': ''
})
'''
#db.drop_table()
#exit(0)

path = '/home/wfgbot/wfgmeetingbot/logs/ChannelLogger/quakenet/#0ad-dev'

'''
23:20 -!- Q sets mode: +o scythetwirler
06:29 -!- Grugnas is now known as Grugnas_afk
'''


re_msg = re.compile(r'(\d{2}:\d{2}) <(.+?)> (.*)')
re_ent = re.compile(r'(\d{2}:\d{2}) -!- (.+) \[(.*?)\] has ([a-z]+)')
re_rnm = re.compile(r'(\d{2}:\d{2}) -!- (.*?) is now known as (.*?)')
re_chg = re.compile(r'(\d{2}:\d{2}) -!- (.+?) changed the topic .*')
re_irs = re.compile(r'(\d{2}:\d{2}) -!- Irssi: .*')
re_oth = re.compile(r'(\d{2}:\d{2}) -!- .*')
re_q = re.compile(r'(\d{2}:\d{2}) -!- .+? sets mode: .*')
re_log = re.compile(r'--- Log .*')
re_day = re.compile(r'--- Day .*')
re_lst = re.compile(r'\d{2}:\d{2} \[.*?\] .*')
re_usr = re.compile(r'\d{2}:\d{2} \[Users .*?\].*')
re_me = re.compile(r'(\d{2}:\d{2})[ ]+?\* (.+?) (.*)')

total_lines = 0
longest_line = ''
longest_user = ''

def process(path):
    global longest_line
    global longest_user
    global total_lines
    handle = open(path)
    data = handle.read()
    handle.close()
    line_count = 0
    channel = os.path.dirname(path).split('/')[-1]
    date = os.path.basename(path)[:10]
    was = False
    for line in data.split('\n'):
        line_count += 1
        new_line = ''
        found = False
        for c in line:
            if ord(c) >= 127:
                found = True
                continue
            new_line += c
        #if found:
        #    if not was:
        #        print(line)
        #        print(new_line)
        #        was = True
        line = new_line
        if not line:
            continue
        if len(line) > len(longest_line):
            longest_line = line
        if re_msg.match(line):
            total_lines += 1
            result = re_msg.findall(line)[0]
            user = result[1].strip()
            if len(user) > len(longest_user):
                longest_user = user
            item = {
                'date': date,
                'time': result[0].strip() + ':00',
                'line': line_count,
                'channel': channel,
                'name': result[1].strip(),
                'type': 'MESSAGE',
                'text': result[2]
            }
            db.add_item(item)
        elif re_ent.match(line):
            #total_lines += 1
            result = re_ent.findall(line)[0]
            ent_type = 'UNKNOWN'
            if result[3] == 'quit' or result[3] == 'left':
                ent_type = 'LEAVE'
            elif result[3] == 'joined':
                ent_type = 'ENTER'
            else:
                print('FAIL')
                print(result[3])
            item = {
                'date': date,
                'time': result[0].strip() + ':00',
                'line': line_count,
                'channel': channel,
                'name': result[1].strip(),
                'type': ent_type,
                'text': result[2]
            }
            #db.add_item(item)
        elif re_me.match(line):
            total_lines += 1
            result = re_me.findall(line)[0]
            item = {
                'date': date,
                'time': result[0].strip() + ':00',
                'line': line_count,
                'channel': channel,
                'name': result[1].strip(),
                'type': 'ME',
                'text': result[2]
            }
            db.add_item(item)
        elif re_rnm.match(line):
            total_lines += 1
            result = re_rnm.findall(line)[0]
            item = {
                'date': date,
                'time': result[0].strip() + ':00',
                'line': line_count,
                'channel': channel,
                'name': result[1].strip(),
                'type': 'CHANGE',
                'text': result[2]
            }
            db.add_item(item)
        elif re_chg.match(line):
            pass
        elif re_q.match(line):
            pass
        elif re_irs.match(line):
            pass
        elif re_log.match(line):
            pass
        elif re_day.match(line):
            pass
        elif re_lst.match(line):
            pass
        elif re_usr.match(line):
            pass
        elif re_oth.match(line):
            pass
        else:
            print(line)
    db.commit_insert()

files = []
for file_name in os.listdir(path):
    file_path = os.path.join(path, file_name)
    files.append(file_path)
files.sort()

for i in range(len(files)):
    print('%d/%d: %s' % (i + 1, len(files), files[i]))
    process(files[i])

print('Total: %d' % total_lines)
print('Longest line: %d' % len(longest_line))
print('Longest user: %d' % len(longest_user))
#print(longest_line)
#print(longest_user)

#db.drop_table()
