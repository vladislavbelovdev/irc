<?php
$uri = urldecode($_SERVER['REQUEST_URI']);
$pattern = '|^/(\d{4}-\d{2}-\d{2})-QuakeNet-([a-z#\-0-9]+?)\.log|';

$found = false;
$content = '';

$base_path = '/home/wfgbot/wfgmeetingbot/logs/ChannelLogger/quakenet/';

if (preg_match($pattern, $uri, $matches)) {
    $date = $matches[1];
    $channel = $matches[2];
    if ($channel == '#0ad-dev' || $channel == '#0ad') {
        $path = $base_path.$channel.'/'.$date.'-QuakeNet-'.$channel.'.log';
        if (file_exists($path)) {
            $content = file_get_contents($path);
            $found = true;
        }
    }
}

if ($found) {
    http_response_code(200);
    header('Content-Type: text/plain');
    echo $content;
} else {
?><!DOCTYPE html>
<html><head><title>404 Not Found</title></head><body>
<h1>Not Found</h1>
<p>The requested URL <?php echo $_SERVER['REQUEST_URI']; ?> was not found on this server.</p>
</body></html><?php
}


