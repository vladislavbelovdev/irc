<?php
require_once($_SERVER['DOCUMENT_ROOT'].'modules/db.php');
require_once($_SERVER['DOCUMENT_ROOT'].'modules/irc.php');
require_once($_SERVER['DOCUMENT_ROOT'].'modules/irc_format.php');

$irc = new IRC();

require_once($_SERVER['DOCUMENT_ROOT'].'modules/header.php');

function get_int($name, $minimum, $maximum, $default) {
    if (!isset($_GET[$name]) || empty($_GET[$name]))
        return $default;
    $value = intval($_GET[$name]);
    if ($value < $minimum)
        $value = $minimum;
    if ($value > $maximum)
        $value = $maximum;
    return $value;
}

$page_size = get_int('page_size', 10, 200, 50);
$page = get_int('page', 0, 10000000 / $page_size, 0);

if (isset($_GET['date']) && !empty($_GET['date'])) {
    irc_format_day($irc->get_by_date($_GET['date']));
} else if (isset($_GET['name']) && !empty($_GET['name'])) {
    irc_format_name($irc->get_by_name($_GET['name'], $page_size, $page));
} else if (isset($_GET['query']) && !empty($_GET['query'])) {
    irc_format_query($irc->get_by_query($_GET['query'], $page_size, $page));
} else {
    irc_format_day($irc->get_last_day());
}

require_once($_SERVER['DOCUMENT_ROOT'].'modules/footer.php');
